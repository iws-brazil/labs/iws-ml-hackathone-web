import React, { Component, Fragment } from 'react';
import styled from 'styled-components';

import { connect } from 'react-redux';
import { fetchAppState, saveSongs, saveSettings, trainModel } from '../../actions'

import { Container, Paper, Grid, Typography, Button, CircularProgress, Box } from '@material-ui/core';

import JSONInput from 'react-json-editor-ajrm';

const StyledContainer = styled(Container)`
&& {
  text-align:left;
}
`
const StyledPaper = styled(Paper)`
&& {
  margin-top: 10px;
  margin-bottom: 20px;
}
`

class ControlPanel extends Component {

  constructor(props) {
    super(props);

    this.state = {
      songsJSON: null,
      settingsJSON: null,
    }

    this.onSongsJSONChange = this.onSongsJSONChange.bind(this);
    this.saveSongsHandler = this.saveSongsHandler.bind(this);

    this.onSettingsJSONChange = this.onSettingsJSONChange.bind(this);
    this.saveSettingsHandler = this.saveSettingsHandler.bind(this);

  }

  componentDidMount() {
    const { fetchAppState } = this.props;

    fetchAppState()
  }

  onSongsJSONChange(data) {
    this.setState({ songsJSON: data.jsObject });
  }

  saveSongsHandler() {
    const { songsJSON } = this.state;
    const { songs, saveSongs } = this.props;


    saveSongs(songsJSON || songs);

    this.setState({ songsJSON: null });
  }

  onSettingsJSONChange(data) {
    this.setState({ settingsJSON: data.jsObject });
  }

  saveSettingsHandler() {
    const { settingsJSON } = this.state;
    const { settings, saveSettings } = this.props;


    saveSettings(settingsJSON || settings);

    this.setState({ settingsJSON: null });
  }

  render() {
    const { settings, songs, appState, model, trainModel, isRequesting, requestAction, requestError } = this.props
    return (
      <Fragment>
        <StyledContainer maxWidth="md">
        <Typography component='h2' variant='h6'>Configurar Músicas</Typography>
        <StyledPaper elevation={2}>
            <Grid container>
              <Grid item xs={8}>
                <div style={{textAlign: 'left', margin: '10px'}}>
                  <JSONInput
                      id='songsJSON'
                      placeholder={this.state.songsJSON || songs}
                      onChange={this.onSongsJSONChange}
                      height='150px'
                      width='100%'
                  />
                </div>
              </Grid>
              <Grid item xs={4}>
                <div style={{textAlign: 'center', marginTop: '50px'}}>
                  { isRequesting && requestAction === 'SAVE_SONGS' ?
                    <CircularProgress color="secondary" />
                    :
                    <Button variant="contained" color="default" onClick={this.saveSongsHandler} >Atualizar</Button>
                  }
                  { requestError && requestAction === 'SAVE_SONGS' &&
                    <Box bgcolor="error.main" color="error.contrastText" p={2} m={1}>
                      {requestError}
                    </Box>
                  }
                </div>
              </Grid>
            </Grid>
          </StyledPaper>
          <Typography component='h2' variant='h6'>Configurações Gerais</Typography>
          <StyledPaper elevation={2}>
            <Grid container>
              <Grid item xs={8}>
                <div style={{textAlign: 'left', margin: '10px'}}>
                  <JSONInput
                      id='settingsJSON'
                      placeholder={this.state.settingsJSON || settings }
                      onChange={this.onSettingsJSONChange}
                      height='150px'
                      width='100%'
                  />
                </div>
              </Grid>
              <Grid item xs={4}>
                <div style={{textAlign: 'center', marginTop: '50px'}}>
                { isRequesting && requestAction === 'SAVE_SETTINGS' ?
                  <CircularProgress color="secondary" />
                  :
                  <Button variant="contained" color="default"onClick={this.saveSettingsHandler}>Atualizar</Button>
                }
                { requestError && requestAction === 'SAVE_SETTINGS' &&
                  <Box bgcolor="error.main" color="error.contrastText" p={2} m={1}>
                    {requestError}
                  </Box>
                }
                </div>
              </Grid>
            </Grid>
          </StyledPaper>
          <Typography component='h2' variant='h6'>Aprendizado</Typography>
          <StyledPaper elevation={2}>
            <Grid container>
              <Grid item xs={12}>
                <div style={{textAlign: 'left', margin: '10px'}}>
                  <JSONInput
                      id='songsJSON'
                      placeholder={ appState }
                      height='300px'
                      width='100%'
                      viewOnly={true}
                      confirmGood={false}
                  />
                </div>
              </Grid>
            </Grid>
            <Grid container>
              <Grid item xs={8}>
                <div style={{textAlign: 'left', margin: '10px'}}>
                  <JSONInput
                      id='settingsJSON'
                      placeholder={model}
                      height='150px'
                      width='100%'
                      viewOnly={true}
                      confirmGood={false}
                  />
                </div>
              </Grid>
              <Grid item xs={4}>
                <div style={{textAlign: 'center', marginTop: '50px'}}>
                  { isRequesting && requestAction === 'TRAIN_MODEL' ?
                    <CircularProgress color="secondary" />
                    :
                    <Button variant="contained" color="secondary" onClick={trainModel} >Treinar Modelo</Button>
                  }
                  { requestError && requestAction === 'TRAIN_MODEL' &&
                    <Box bgcolor="error.main" color="error.contrastText" p={2} m={1}>
                      {requestError}
                    </Box>
                  }
                </div>
              </Grid>
            </Grid>
          </StyledPaper>
        </StyledContainer>
      </Fragment>
    );
  };
}

const mapStateToProps = ({ isRequesting, requestAction, requestError, songs, mode, expectedBeats, sampleSize, model }) => {

  const settings = {
    mode,
    expectedBeats,
    sampleSize
  }

  const songNames = songs.allIds.map((sId) => {
    return { name: songs.byId[sId].name }
  });

  return {
    isRequesting,
    requestAction,
    requestError,
    songs:songNames,
    settings,
    appState: {
      songs,
      mode,
      expectedBeats,
      sampleSize
    },
    model
  }
};

const mapDispatchToProps = {
  fetchAppState,
  saveSongs,
  saveSettings,
  trainModel
};
ControlPanel = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ControlPanel);


export default ControlPanel;
