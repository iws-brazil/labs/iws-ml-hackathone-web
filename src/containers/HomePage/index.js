import React, { Component, Fragment } from 'react';
import styled from 'styled-components';

import { connect } from 'react-redux';
import { startPolling, stopPolling, fetchAppState, saveBeatSequence, predict, clearPrediction } from '../../actions'

import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';

import DataCollect from '../../components/DataCollect';
import DataTest from '../../components/DataTest';

const StyledContainer = styled(Container)`
  margin-top: 10px;
  text-align: center;
`;

class HomePage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      beatCache: [],
      lastBeat: null,
      chartData: [],
      predictions: {}
    }

    this.onCollectBeatHandler = this.onCollectBeatHandler.bind(this);
    this.onTestBeatHandler = this.onTestBeatHandler.bind(this);
  }


  componentDidMount() {
    this.props.fetchAppState();
    this.props.startPolling(5000);

    setInterval(() => {
      this.setState({
        chartData: this.getChartData(this.state.beatCache)
      })
    }, 300);
  }

  getChartData(beatBuffer) {
    const WINDOW_SIZE = 10000; //10 seconds
    const BAR_COUNT = 100;
    const windowEnd = new Date().getTime();
    const windowBegin = windowEnd - WINDOW_SIZE;

    const data = [];

    //Initial Point
    data.push({
      x: 0,
      y: 0
    })

    data.push({
      x: 0,
      y: 100
    })
    for(let i = 1; i < BAR_COUNT; i++ ){
      data.push({
        x: i,
        y: 20
      })
    }
    data.push({
      x: BAR_COUNT,
      y: 100
    })

    for(let beatSequence of beatBuffer) {
      for(let beat of beatSequence) {
        if(beat > windowBegin) {
          let perc = (beat - windowBegin) / WINDOW_SIZE
          let barIndex = 1 + Math.floor(perc*BAR_COUNT);
          data[barIndex].y = 100;
        }
      }
    }


    // for(let i = 0; i< CHART_WINDOW_SIZE; i++) {
    //   data.push({
    //     x: i,
    //     y: Math.round(Math.random())
    //   })
    // }

    return data;
  }

  onCollectBeatHandler() {
    const SEGMENT_TOLERANCE = 3000; //Beats within 3 secs apart are from same segment

    const {beatCache} = this.state;
    const {saveBeatSequence, currentSong} = this.props;
    let beatTime = new Date().getTime();

    // get current Song Segment
    if(beatCache.length === 0)
      beatCache.push([]);

    beatCache[beatCache.length - 1].push(beatTime);

    this.setState({lastBeat: beatTime, beatCache});

    clearTimeout(this.sequenceTimer);
    this.sequenceTimer = setTimeout(() => {
      //Submit Sequence
      saveBeatSequence(currentSong.id, beatCache[beatCache.length - 1])

      beatCache.push([]);

      clearTimeout(this.sequenceTimer);
    },SEGMENT_TOLERANCE)
  }

  onTestBeatHandler() {
    const SEGMENT_TOLERANCE = 5000; //Beats within 3 secs apart are from same segment

    const { sampleSize, predict, clearPrediction } = this.props;
    const { beatCache } = this.state;
    let beatTime = new Date().getTime();

    // get current Song Segment
    if(beatCache.length === 0)
      beatCache.push([]);

    beatCache[beatCache.length - 1].push(beatTime);

    this.setState({lastBeat: beatTime, beatCache});

    if(!this.predictTimer) {
      this.predictTimer = setInterval(() => {
        let currentSequence = beatCache[beatCache.length - 1];
        if(currentSequence.length >= sampleSize + 2) {
          predict(currentSequence);
        }
      },3000);
    }

    clearTimeout(this.sequenceTimer);
    this.sequenceTimer = setTimeout(() => {
      beatCache.push([]);

      clearTimeout(this.sequenceTimer);
      clearInterval(this.predictTimer);

      this.sequenceTimer = null;
      this.predictTimer = null;

      clearPrediction();
    },SEGMENT_TOLERANCE)
  }

  render() {
    const { completedSongs, currentSong, mode, expectedBeats, requestError, songs, prediction } = this.props;

    return (
      <Fragment>
        { requestError &&
          <Box bgcolor="error.main" color="error.contrastText" p={2} m={1}>
            {requestError}
          </Box>
        }
        <StyledContainer maxWidth="md">
          { mode === 'collect' &&
            <DataCollect completedSongs={completedSongs} currentSong={currentSong} expectedBeats={expectedBeats} chartData={this.state.chartData} onBeat={this.onCollectBeatHandler}/>
          }
          { mode === 'test' &&
            <DataTest songs={songs} prediction={prediction} chartData={this.state.chartData} onBeat={this.onTestBeatHandler}/>
          }
        </StyledContainer>
      </Fragment>
    );
  };
}

const mapStateToProps = ({ requestError, songs, mode, expectedBeats, sampleSize, prediction }) => {
  let completedSongs = songs.allIds.map((id) => songs.byId[id]).filter((s) => {
    return s.registeredBeats >= expectedBeats;
  });

  let nonCompletedSongs = songs.allIds.map((id) => songs.byId[id]).filter((s) => {
    return s.registeredBeats < expectedBeats;
  });

  let currentSong = nonCompletedSongs.length > 0 ? nonCompletedSongs[0] : null;

  return {
    songs,
    completedSongs,
    currentSong,
    mode,
    expectedBeats,
    sampleSize,
    requestError,
    prediction
  }
};

const mapDispatchToProps = {
  startPolling,
  stopPolling,
  fetchAppState,
  saveBeatSequence,
  predict,
  clearPrediction,
};

HomePage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomePage);


export default HomePage;
