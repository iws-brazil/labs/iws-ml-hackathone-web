import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import HomePage from '../HomePage';
import ControlPanel from '../ControlPanel';
import NotFoundPage from '../NotFoundPage';

import GlobalStyle from '../../global-styles';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import history from '../../utils/history';

import '../../../node_modules/react-vis/dist/style.css';

const theme = createMuiTheme({});

export default function App() {
  return (
    <div>
      <MuiThemeProvider theme={theme}>
        <GlobalStyle />
        <Router history={history}>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/control-panel" component={ControlPanel} />
          <Route component={NotFoundPage} />
        </Switch>
        <GlobalStyle />
      </Router>
    </MuiThemeProvider>
    </div>
  );
}
