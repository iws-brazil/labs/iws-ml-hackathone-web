const defaultStateList = {
  isRequesting: false,
  requestAction:'',
  requestError: null,
  isPolling: false,
  mode: 'collect', //collect or test
  expectedBeats: 0,
  sampleSize: 0,
  songs:{
    byId: {},
    allIds: []
  },
  beatBuffer: [],
  model: {},
  prediction: null,
};

const reducer = (state = defaultStateList, action) => {
    switch (action.type) {
      case 'START_POLLING':
        return {...state, isPolling: true};
      case 'STOP_POLLING':
        return {...state, isPolling: false};
      case 'API_REQUEST_FAILURE':
        return {...state, isRequesting:false, requestError: action.error};
      case 'FETCH_APP_STATE':
        return {...state, isRequesting:true, requestAction:'FETCH_APP_STATE'};
      case 'FETCH_APP_STATE_SUCCESS':
        return {
            ...state,
            isRequesting:false,
            requestError: null,
            songs: {
              byId: action.data.songs,
              allIds: Object.keys(action.data.songs)
            },
            mode:action.data.mode,
            expectedBeats:action.data.expectedBeats,
            sampleSize:action.data.sampleSize,
          };
      case 'SAVE_BEAT_SEQUENCE':
        return {...state, isRequesting:true, requestAction:'SAVE_BEAT_SEQUENCE'};
      case 'SAVE_BEAT_SEQUENCE_SUCCESS':
        return {
          ...state,
          isRequesting:false,
          requestError: null,
          songs: {
            byId: action.data.songs,
            allIds: Object.keys(action.data.songs)
          },
        };
      case 'SAVE_SONGS':
        return {...state, isRequesting:true, requestAction:'SAVE_SONGS'};
      case 'SAVE_SONGS_SUCCESS':
        return {
          ...state,
          isRequesting:false,
          requestError: null,
          songs: {
            byId: action.data.songs,
            allIds: Object.keys(action.data.songs)
          },
        };
      case 'SAVE_SETTINGS':
        return {...state, isRequesting:true, requestAction:'SAVE_SETTINGS'};
      case 'SAVE_SETTINGS_SUCCESS':
        return {
          ...state,
          isRequesting:false,
          requestError: null,
          mode:action.data.mode,
          expectedBeats:action.data.expectedBeats,
          sampleSize:action.data.sampleSize,
        };
      case 'TRAIN_MODEL':
        return {...state, isRequesting:true, requestAction:'TRAIN_MODEL'};
      case 'TRAIN_MODEL_SUCCESS':
        return {
          ...state,
          isRequesting:false,
          requestError: null,
          model:action.data,
        };
      case 'PREDICT':
        return {...state, isRequesting:true, requestAction:'PREDICT'};
      case 'PREDICT_SUCCESS':
        return {
          ...state,
          isRequesting:false,
          requestError: null,
          prediction:action.data,
        };
      case 'CLEAR_PREDICTION':
        return {
          ...state,
          prediction: null,
          };
      default:
        return state;
    }
  };

  export default reducer;
