const axios = require('axios');
const apiUrl = process.env.REACT_APP_API_URL;

export const startPolling = (interval) => {
  return (dispatch, getState) => {
    dispatch({
      type: 'START_POLLING'
    });

    var timer = setInterval(() => {
      let state = getState();
      if(!state.isPolling) {
        clearInterval(timer);
        timer = null;
        return;
      }
      dispatch(fetchAppState());
    }, interval);
  }
}


export const stopPolling = () => ({
  type: 'STOP_POLLING',
});

export const apiRequestFailure = (error) => ({
  type: 'API_REQUEST_FAILURE',
  error
});

export const fetchAppState = () => {
  return async dispatch => {
    try {
      dispatch({
        type: 'FETCH_APP_STATE'
      })
      let response = await axios.get(apiUrl);
      return dispatch(fetchAppStateSuccess(response.data));
    }
    catch(e) {
      return dispatch(apiRequestFailure(e.message));
    }
  }
};

export const fetchAppStateSuccess = (data) => ({
  type: 'FETCH_APP_STATE_SUCCESS',
  data
});

export const saveBeatSequence = (songId, sequence) => {
  return async dispatch => {
    try {
      dispatch({
        type: 'SAVE_BEAT_SEQUENCE'
      })
      let response = await axios.post(`${apiUrl}/songs/${songId}/sequences`, sequence);
      return dispatch(saveBeatSequenceSuccess(response.data));
    }
    catch(e) {
      return dispatch(apiRequestFailure(e.message));
    }
  }
};

export const saveBeatSequenceSuccess = (data) => ({
  type: 'SAVE_BEAT_SEQUENCE_SUCCESS',
  data
});

export const saveSongs = (songs) => {
  return async dispatch => {
    try {
      dispatch({
        type: 'SAVE_SONGS'
      })
      let response = await axios.post(`${apiUrl}/songs`, songs);
      return dispatch(saveSongsSuccess(response.data));
    }
    catch(e) {
      return dispatch(apiRequestFailure(e.message));
    }
  }
}

export const saveSongsSuccess = (data) => ({
  type: 'SAVE_SONGS_SUCCESS',
  data
});

export const saveSettings = (songs) => {
  return async dispatch => {
    try {
      dispatch({
        type: 'SAVE_SETTINGS'
      })
      let response = await axios.post(`${apiUrl}/settings`, songs);
      return dispatch(saveSettingsSuccess(response.data));
    }
    catch(e) {
      return dispatch(apiRequestFailure(e.message));
    }
  }
}

export const saveSettingsSuccess = (data) => ({
  type: 'SAVE_SETTINGS_SUCCESS',
  data
});

export const trainModel = () => {
  return async dispatch => {
    try {
      dispatch({
        type: 'TRAIN_MODEL'
      })
      let response = await axios.post(`${apiUrl}/train-model`);
      return dispatch(trainModelSuccess(response.data));
    }
    catch(e) {
      return dispatch(apiRequestFailure(e.message));
    }
  }
}

export const trainModelSuccess = (data) => ({
  type: 'TRAIN_MODEL_SUCCESS',
  data
});

export const predict = (sampleArray) => {
  return async dispatch => {
    try {
      dispatch({
        type: 'PREDICT'
      })
      let response = await axios.post(`${apiUrl}/predict`, sampleArray);
      return dispatch(predictSuccess(response.data));
    }
    catch(e) {
      return dispatch(apiRequestFailure(e.message));
    }
  }
}

export const predictSuccess = (data) => ({
  type: 'PREDICT_SUCCESS',
  data
});

export const clearPrediction = (data) => ({
  type: 'CLEAR_PREDICTION'
});
