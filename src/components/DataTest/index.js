import React from 'react';
import styled from 'styled-components';

import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import {XYPlot, makeWidthFlexible, VerticalBarSeries} from 'react-vis';

const StyledCardMedia = styled(CardMedia)`
  && {
    object-fit: contain;
    object-position: top;
    height: 20vh;
    background-color: black;
  }
`;

const StyledCardActions = styled(CardActions)`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledLargeButton = styled(Button)`
  && {
    margin: 10px 0 30px;
    padding: 27px 71px;
    font-size: 1.9375rem;
  }
`;

const FeedbackCardContent = styled(CardContent)`
  && {
    background-color: #e5e5e5;
    border: 1px #aaa;
    padding: 50px 20px;
    margin: 0 10%;
    min-height: 100px;
  }
`

const FlexibleXYPlot = makeWidthFlexible(XYPlot);

const DataTest = ({ songs, chartData, onBeat, prediction }) => {
    return (

          <Card>
            <CardContent>
              <StyledCardMedia
                component="img"
                alt="Contemplative Reptile"
                image="https://hack-a-tone.iwsbrazil.io/static/media/hacka-a-tone-logo.23025867.png"
                title="Hack-a-tone"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Machine Learning Prático - Bora batucar?
                </Typography>
                <Typography gutterBottom variant="caption" component="p">
                  Vamos ver se a máquina aprendeu?
                </Typography>
              </CardContent>
              <FlexibleXYPlot height={25} margin={{left: 0, right: 0, top: 10, bottom: 0}}>
                <VerticalBarSeries color={'#f50057'} data={chartData} />
              </FlexibleXYPlot>
            </CardContent>
            <FeedbackCardContent>
              { prediction ?
              (
              <Grid container alignContent="center" alignItems="center">

                <Grid item md={4} xs={12} >
                  <Typography variant="h1" component="span" color="secondary">
                    { Math.floor(prediction.probability * 100) }%
                  </Typography>
                </Grid>
                <Grid item md={8} xs={12}>

                  <Typography variant="h6" component="span" color="primary">
                    De certeza que esta música é <Typography variant="h4" component="p" color="secondary"><strong>{ songs.byId[prediction.index].name }</strong></Typography>
                  </Typography>
                </Grid>
              </Grid>
              ) : (
                <Grid container alignContent="center" alignItems="center">
                  <Grid item md={12} xs={12} >
                    <span role="img" aria-label="Robot Thinking"style={{fontSize: 60, display: 'block'}}>🤖💭</span>
                  </Grid>
                </Grid>
              )
              }
            </FeedbackCardContent>
            <StyledCardActions>
              <StyledLargeButton variant="contained" color="secondary" onClick={onBeat}>
                <span role="img" aria-label="Batuque Aqui" style={{fontSize: 60}}>🥁</span>
              </StyledLargeButton>
            </StyledCardActions>
          </Card>

    );
  };

  export default DataTest;
