import React from 'react';
import styled from 'styled-components';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import {XYPlot, makeWidthFlexible, VerticalBarSeries} from 'react-vis';

import SongList from '../../components/SongList'

const StyledCardMedia = styled(CardMedia)`
  && {
    object-fit: contain;
    object-position: top;
    height: 20vh;
    background-color: black;
  }
`;

const StyledCardActions = styled(CardActions)`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledLargeButton = styled(Button)`
  && {
    margin: 10px 0 30px;
    padding: 27px 71px;
    font-size: 1.9375rem;
  }
`;

const TopCardContent = styled(CardContent)`
  && {
    padding-bottom: 0;
  }
`;

const FlexibleXYPlot = makeWidthFlexible(XYPlot);

const DataCollect = ({ currentSong, expectedBeats, completedSongs, chartData, onBeat }) => {
    return (

          <Card>
            <CardContent>
              <StyledCardMedia
                component="img"
                alt="Contemplative Reptile"
                image="https://hack-a-tone.iwsbrazil.io/static/media/hacka-a-tone-logo.23025867.png"
                title="Hack-a-tone"
              />
              <TopCardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Machine Learning Prático - Bora batucar?
                </Typography>
                <Typography gutterBottom variant="caption" component="p">
                  Todo mundo batucando até encher a barrinha rosa ;)
                </Typography>
              </TopCardContent>
              { currentSong != null ?
                (
                <SongList songs={[ currentSong ]} expectedBeats={expectedBeats}/>
                ) : (
                  <span>You are all set ;)</span>
                )
              }
              <FlexibleXYPlot height={25} margin={{left: 0, right: 0, top: 10, bottom: 0}}>
                <VerticalBarSeries color={'#f50057'} data={chartData} />
              </FlexibleXYPlot>
            </CardContent>
            { currentSong != null &&
            <StyledCardActions>
              <StyledLargeButton variant="contained" color="secondary" onMouseDown={onBeat}>
                <span role="img" aria-label="Batuque Aqui" style={{fontSize: 60}}>🥁</span>
              </StyledLargeButton>
            </StyledCardActions>
            }
            <CardContent>
            <SongList songs={completedSongs} expectedBeats={expectedBeats}/>
            </CardContent>
          </Card>

    );
  };

  export default DataCollect;
