import React from 'react';
import styled from 'styled-components';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import LinearProgress from '@material-ui/core/LinearProgress';
import MusicNoteIcon from '@material-ui/icons/MusicNote';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';

const PinkAvatar = styled(Avatar)`
  && {
    background-color: #f50057;
  }
`;

const GreenAvatar = styled(Avatar)`
  && {
    background-color: #00e676;
  }
`;

const CounterListItemText = styled(ListItemText)`
  && {
    flex-grow: 0;
    margin-left: 10px;

    span {
      font-size: 10px;
      text-transform: uppercase;
    }
  }
`;

const GreenProgressBar = styled(LinearProgress)`
  && {
    .MuiLinearProgress-bar {
      background-color: #00e676;
    }
  }
`;

const GreenCheckIcon = styled(CheckCircleOutlineIcon)`
  && {
    color: #00e676;
    font-size: 30px;
    margin: 0 13px;
  }
`;

const SongList = ({ songs, expectedBeats }) => {
    return (
      <List>
        {songs.map(song => (
           <ListItem key={song.id}>
            <ListItemAvatar>
              { song.registeredBeats >= expectedBeats ?
                (
                  <GreenAvatar>
                    <MusicNoteIcon />
                  </GreenAvatar>
                ) : (
                  <PinkAvatar>
                    <MusicNoteIcon />
                  </PinkAvatar>
                )
              }
            </ListItemAvatar>
            <ListItemText>
              {song.name}
              { song.registeredBeats >= expectedBeats ?
                (
                  <GreenProgressBar variant="determinate" value={100}/>
                ) : (
                  <LinearProgress variant="determinate" value={Math.floor(100*song.registeredBeats/expectedBeats)} color="secondary" />
                )
              }
            </ListItemText>
            { song.registeredBeats >= expectedBeats ?
                (
                  <GreenCheckIcon />
                ) : (
                  <CounterListItemText> {song.registeredBeats} / {expectedBeats} beats</CounterListItemText>
                )
              }
          </ListItem>
        ))}

      </List>
    );
  };

  export default SongList;
